import 'package:flutter/widgets.dart';

class Width extends SizedBox {
  Width(double width) : super(width: width, height: 0);
}

class Height extends SizedBox {
  Height(double height) : super(width: 0, height: height);
}
