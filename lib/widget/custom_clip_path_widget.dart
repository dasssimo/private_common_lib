import 'package:flutter/widgets.dart';

class CustomClipPathWidget extends StatelessWidget {
  final Color color;
  final Path path;
  final Widget child;

  CustomClipPathWidget({@required this.path, this.color, this.child});

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: _CustomPathClipper(path),
      child: Container(
        color: color,
        child: child,
      ),
    );
  }
}

class _CustomPathClipper extends CustomClipper<Path> {
  final Path path;

  _CustomPathClipper(this.path);

  @override
  Path getClip(Size size) => path;

  @override
  bool shouldReclip(_CustomPathClipper oldClipper) => false;
}
