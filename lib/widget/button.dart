import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CircleButton extends StatelessWidget {
  final Color color;
  final Widget child;
  final VoidCallback onPressed;

  CircleButton({
    this.color,
    @required this.child,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.zero,
      minWidth: 0,
      child: child,
      onPressed: onPressed,
      color: color,
      shape: CircleBorder(),
    );
  }
}
