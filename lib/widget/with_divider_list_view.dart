import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class WithDividerListView extends ListView {
  WithDividerListView({
    @required int itemCount,
    @required IndexedWidgetBuilder itemBuilder,
    Color dividerColor = Colors.grey,
  }) : super.builder(
    itemCount: 2 * itemCount - 1,
    itemBuilder: (context, index) {
      if (index % 2 == 0) {
        return itemBuilder(context, index ~/ 2);
      } else {
        return Container(
          height: 1,
          color: dividerColor,
        );
      }
    },
  );
}
