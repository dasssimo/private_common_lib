import 'dart:math';

import 'package:flutter/widgets.dart';

abstract class CustomGestureDetectorCallback {
  void onFirstTouchStart();

  void onTouchStart(int numTouchPoint, List<Point<double>> touchPoints);

  void onTouchUpdate(int numTouchPoint, List<Point<double>> touchPoints);

  void onTouchEnd(int numTouchPoint, List<Point<double>> touchPoints);

  void onLastTouchEnd();
}

class CustomGestureDetectorWidget extends StatefulWidget {
  final Widget child;
  final void Function() onFirstTouchStart;
  final void Function(int numTouchPoint, List<Point<double>> touchPoints) onTouchStart;
  final void Function(int numTouchPoint, List<Point<double>> touchPoints) onTouchUpdate;
  final void Function(int numTouchPoint, List<Point<double>> touchPoints) onTouchEnd;
  final void Function() onLastTouchEnd;

  final CustomGestureDetectorCallback callback;

  final HitTestBehavior hitTestBehavior;

  CustomGestureDetectorWidget({
    this.child,
    this.onFirstTouchStart,
    this.onTouchStart,
    this.onTouchUpdate,
    this.onTouchEnd,
    this.onLastTouchEnd,
    this.callback,
    this.hitTestBehavior = HitTestBehavior.deferToChild,
  });

  @override
  State<StatefulWidget> createState() => CustomGestureDetectorWidgetState();
}

class CustomGestureDetectorWidgetState extends State<CustomGestureDetectorWidget> {
  int _numTouchPoint = 0;
  Map<int, Point<double>> _updatedTouchPoints = Map();

  @override
  Widget build(BuildContext context) {
    RenderBox renderBox;

    WidgetsBinding.instance.addPostFrameCallback((_) {
      renderBox = context.findRenderObject();
    });

    Point<double> _getTouchPoint(Offset focalPoint) {
      final offset = renderBox.globalToLocal(focalPoint);
      return Point(offset.dx, offset.dy);
    }

    void Function() onFirstTouchStart;
    void Function(int numTouchPoint, List<Point<double>> touchPoints) onTouchStart;
    void Function(int numTouchPoint, List<Point<double>> touchPoints) onTouchUpdate;
    void Function(int numTouchPoint, List<Point<double>> touchPoints) onTouchEnd;
    void Function() onLastTouchEnd;

    final callback = widget.callback;
    if (callback != null) {
      onFirstTouchStart = callback.onFirstTouchStart;
      onTouchStart = callback.onTouchStart;
      onTouchUpdate = callback.onTouchUpdate;
      onTouchEnd = callback.onTouchEnd;
      onLastTouchEnd = callback.onLastTouchEnd;
    } else {
      onFirstTouchStart = widget.onFirstTouchStart;
      onTouchStart = widget.onTouchStart;
      onTouchUpdate = widget.onTouchUpdate;
      onTouchEnd = widget.onTouchEnd;
      onLastTouchEnd = widget.onLastTouchEnd;
    }

    return Listener(
      child: widget.child,
      onPointerDown: (event) {
        if (_numTouchPoint == 0) {
          if (onFirstTouchStart != null) {
            onFirstTouchStart();
          }
        } else {
          if (onTouchEnd != null) {
            onTouchEnd(_numTouchPoint, _updatedTouchPoints.values.toList());
          }
        }
        _numTouchPoint++;
        _updatedTouchPoints[event.device] = _getTouchPoint(event.position);
        if (onTouchStart != null) {
          onTouchStart(_numTouchPoint, _updatedTouchPoints.values.toList());
        }
      },
      onPointerMove: (event) {
        if (_updatedTouchPoints.length == _numTouchPoint) {
          _updatedTouchPoints.clear();
        }
        _updatedTouchPoints[event.device] = _getTouchPoint(event.position);
        if (_updatedTouchPoints.length == _numTouchPoint) {
          if (onTouchUpdate != null) {
            onTouchUpdate(_numTouchPoint, _updatedTouchPoints.values.toList());
          }
        }
      },
      onPointerUp: (event) {
        if (onTouchEnd != null) {
          onTouchEnd(_numTouchPoint, _updatedTouchPoints.values.toList());
        }
        _updatedTouchPoints.remove(event.device);
        _numTouchPoint--;
        if (_numTouchPoint == 0) {
          if (onLastTouchEnd != null) {
            onLastTouchEnd();
          }
          reset();
        } else {
          if (onTouchStart != null) {
            onTouchStart(_numTouchPoint, _updatedTouchPoints.values.toList());
          }
        }
      },
      onPointerCancel: (event) {
        reset();
      },
      behavior: widget.hitTestBehavior,
    );
  }

  void reset() {
    _numTouchPoint = 0;
    _updatedTouchPoints.clear();
  }
}
