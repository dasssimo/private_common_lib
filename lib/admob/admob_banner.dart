import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:connectivity/connectivity.dart';
import 'package:private_common_lib/admob/admob_config.dart';

enum _Status { idle, loading, loaded, show }

_Status _status = _Status.idle;

BannerAd _bannerAd;

void _tryShow(AdMobConfig config) {
  if (_bannerAd == null) {
    _bannerAd = BannerAd(
        targetingInfo: MobileAdTargetingInfo(
          testDevices: config.targetDevices,
        ),
        adUnitId: config.adUnitId,
        size: AdSize.banner,
        listener: (event) {
          switch (event) {
            case MobileAdEvent.loaded:
              _status = _Status.loaded;
              break;
            case MobileAdEvent.failedToLoad:
            case MobileAdEvent.closed:
              _status = _Status.idle;
              break;
            default:
              break;
          }
        });
  }
  if (_status == _Status.idle) {
    Future(() async {
      if (await Connectivity().checkConnectivity() != ConnectivityResult.none) {
        final result = await _bannerAd.load();
        if (result) {
          _status = _Status.loading;
        }
      }
    });
  } else if (_status == _Status.loaded) {
    _bannerAd.show(anchorType: AnchorType.bottom);
    _status = _Status.show;
  }
}

class BannerProviderWidget extends StatelessWidget {
  final Widget Function(BuildContext) builder;
  final AdMobConfig config;
  final Color color;

  BannerProviderWidget({
    @required this.builder,
    @required this.config,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    _tryShow(config);

    Widget child = builder(context);
    if (_status == _Status.show) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: child,
          ),
          Container(
            color: color,
            height: AdSize.banner.height.toDouble(),
          ),
        ],
      );
    } else {
      return child;
    }
  }
}
