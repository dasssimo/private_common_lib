import 'package:connectivity/connectivity.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/widgets.dart';
import 'package:private_common_lib/admob/admob_config.dart';

enum RewardVideoErrorType { notConnectNetwork, failedLoadAd }

void tryShowVideoReward(
  BuildContext context,
  AdMobConfig config,
  void Function(bool isRewarded) onClosed,
  void Function(RewardVideoErrorType errorType) onError,
) async {
  final rewardVideoAd = RewardedVideoAd.instance;
  var isRewarded = false;

  void finish() {
    rewardVideoAd.listener = null;
  }

  void error(RewardVideoErrorType errorType) {
    onError(errorType);
    finish();
  }

  if (await Connectivity().checkConnectivity() == ConnectivityResult.none) {
    error(RewardVideoErrorType.notConnectNetwork);
  }

  Future load() {
    return rewardVideoAd.load(
      adUnitId: config.adUnitId,
      targetingInfo: MobileAdTargetingInfo(
        testDevices: config.targetDevices,
      ),
    );
  }

  rewardVideoAd.listener = (RewardedVideoAdEvent event, {String rewardType, int rewardAmount}) {
    switch (event) {
      case RewardedVideoAdEvent.rewarded:
        isRewarded = true;
        break;
      case RewardedVideoAdEvent.closed:
        onClosed(isRewarded);
        load();
        finish();
        break;
      default:
        break;
    }
  };

  var hasError = false;

  await load().catchError((_) {
    hasError = true;
  });
  if (hasError) {
    error(RewardVideoErrorType.failedLoadAd);
    return;
  }

  await rewardVideoAd.show().catchError((_) {
    hasError = true;
  });
  if (hasError) {
    error(RewardVideoErrorType.failedLoadAd);
    return;
  }
}
