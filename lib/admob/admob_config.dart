class AdMobConfig {
  final List<String> targetDevices;
  final String adUnitId;

  const AdMobConfig(this.targetDevices, this.adUnitId);
}