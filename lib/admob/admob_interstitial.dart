import 'package:connectivity/connectivity.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/widgets.dart';
import 'package:private_common_lib/admob/admob_config.dart';

Future<void> tryShowLoadInterstitial(
  BuildContext context,
  AdMobConfig config,
  VoidCallback onShow,
  VoidCallback onError,
) async {
  if (await Connectivity().checkConnectivity() == ConnectivityResult.none) {
    onError();
    return;
  }

  final ad = InterstitialAd(
    adUnitId: config.adUnitId,
    targetingInfo: MobileAdTargetingInfo(
      testDevices: config.targetDevices,
    ),
    listener: (event) {
      if (event == MobileAdEvent.opened) {
        onShow();
      }
    },
  );

  bool hasError = false;

  await ad.load().catchError((_) {
    hasError = true;
  });
  if (hasError) {
    onError();
    return;
  }

  await ad.show(anchorOffset: 0.0, anchorType: AnchorType.bottom).catchError((_) {
    hasError = true;
  });
  if (hasError) {
    onError();
    return;
  }
}
