import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LoadingIndicatorDialog {
  final BuildContext context;
  final bool canCancelBackKey;

  var _hasDialog = false;

  LoadingIndicatorDialog({
    @required this.context,
    this.canCancelBackKey = true,
  });

  void show() {
    _hasDialog = true;
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return WillPopScope(
          onWillPop: () {
            return Future(() {
              return canCancelBackKey;
            });
          },
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    ).whenComplete(() {
      _hasDialog = false;
    });
  }

  void dismiss() {
    if (_hasDialog) {
      Navigator.pop(context);
      _hasDialog = false;
    }
  }
}
