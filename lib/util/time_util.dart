import 'dart:async';

Timer wait(int millisecond, void Function() callback) =>
    Timer(Duration(milliseconds: millisecond), callback);
