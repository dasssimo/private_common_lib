T limit<T extends num>(T min, T target, T max) {
  if (min > max) {
    throw Exception("min:$min is bigger than max:$max.");
  }
  if (target < min) {
    return min;
  }
  if (target > max) {
    return max;
  }
  return target;
}
