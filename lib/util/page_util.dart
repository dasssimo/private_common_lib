import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void pushPage(
  BuildContext context,
  Widget page,
) {
  Navigator.of(context).push(
    MaterialPageRoute<Null>(
      builder: (context) {
        return page;
      },
    ),
  );
}

void pushReplacementPage(
  BuildContext context,
  Widget page,
) {
  Navigator.of(context).pushReplacement(
    MaterialPageRoute<Null>(
      builder: (context) {
        return page;
      },
    ),
  );
}
