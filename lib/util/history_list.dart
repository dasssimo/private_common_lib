class HistoryList<E> {
  static const maxSize = 30;

  final List<E> list = List<E>();

  int _referenceCounter;

  int get _lastReferenceCounter => (list.isNotEmpty ? list.length - 1 : null);

  bool get _isReferenceLast =>
      (_referenceCounter != null && _referenceCounter == _lastReferenceCounter);

  E get reference => (_referenceCounter != null ? list[_referenceCounter] : null);

  void add(E element) {
    if (reference == element) {
      return;
    }

    if (list.isNotEmpty && !_isReferenceLast) {
      list.removeRange(_referenceCounter + 1, list.length);
    }
    if (list.length == maxSize) {
      list.removeAt(0);
    }
    list.add(element);
    _referenceCounter = _lastReferenceCounter;
  }

  void reset() {
    list.clear();
    _referenceCounter = null;
  }

  bool get canMoveBack => list.isNotEmpty && _referenceCounter > 0;

  void moveBack() {
    if (canMoveBack) {
      _referenceCounter--;
    }
  }

  bool get canMoveForward => list.isNotEmpty && _referenceCounter < list.length - 1;

  void moveForward() {
    if (canMoveForward) {
      _referenceCounter++;
    }
  }
}
