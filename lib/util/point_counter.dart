class PointCounter {
  final int maxPoint;

  int _point;

  PointCounter(this.maxPoint) {
    reset();
  }

  bool get isAchieved => _point >= maxPoint;

  void add(int addPoint) {
    _point += addPoint;
  }

  void reset() {
    _point = 0;
  }
}
