import 'dart:async';
import 'dart:math';

typedef TimeNotifierCallback = void Function(int);

class TimeNotifier {
  final _stopwatch = Stopwatch();
  final int maxTime;
  final TimeNotifierCallback callback;

  var _sec = 0;
  var _baseTime = 0.0;

  Timer _timer;

  TimeNotifier({
    this.maxTime = 24 * 3600,
    this.callback,
  });

  int get sec => _sec;

  void setTime(double time) {
    _baseTime = time;
    _stopwatch.reset();
  }

  set time(double time) {
    _baseTime = time;
    _stopwatch.reset();
  }

  double get time => _stopwatch.elapsedMilliseconds / 1000 + _baseTime;

  void start() {
    _timer?.cancel();
    _timer = Timer.periodic(
      Duration(milliseconds: 100),
      (Timer timer) {
        final sec = min(time.toInt(), maxTime);
        if (sec > _sec) {
          _sec = sec;
          callback(_sec);
        }
      },
    );
    _stopwatch.start();
  }

  void stop() {
    _stopwatch.stop();
    _baseTime += _stopwatch.elapsedMilliseconds;
    _stopwatch.reset();
    _timer?.cancel();
  }

  void reset() {
    stop();
    _stopwatch.reset();
    _sec = 0;
    _baseTime = 0;
    callback(_sec);
  }
}
